package com.example.greenleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  GreenleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenleafApplication.class, args);
	}

}
