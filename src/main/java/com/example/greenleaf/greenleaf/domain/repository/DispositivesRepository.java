package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Dispositives;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DispositivesRepository extends MongoRepository<Dispositives, String> {
    //Optional<Dispositives> findByElectrod(String name);
}
