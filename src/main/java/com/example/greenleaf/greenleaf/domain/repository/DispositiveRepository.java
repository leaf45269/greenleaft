package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Dispositive;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DispositiveRepository extends MongoRepository<Dispositive, String> {
    Optional<Dispositive> findByDispositive(String name);
}
