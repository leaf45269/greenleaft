package com.example.greenleaf.greenleaf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Building {
    @Id
    private String id;
    private String name;
    private String addres;
    private String date;
    private String electricityBillId;
    private String waterBillId;
    private String gasBillId;
    private String state;
    private Integer floors;
    private Integer rooms;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getElectricityBillId() {
        return electricityBillId;
    }

    public void setElectricityBillId(String electricityBillId) {
        this.electricityBillId = electricityBillId;
    }

    public String getWaterBillId() {
        return waterBillId;
    }

    public void setWaterBillId(String waterBillId) {
        this.waterBillId = waterBillId;
    }

    public String getGasBillId() {
        return gasBillId;
    }

    public void setGasBillId(String gasBillId) {
        this.gasBillId = gasBillId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }
}
