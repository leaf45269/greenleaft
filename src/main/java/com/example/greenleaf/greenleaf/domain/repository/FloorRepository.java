package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Floor;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface FloorRepository extends MongoRepository<Floor, String> {
    Optional<Floor> findByName(String name);
}
