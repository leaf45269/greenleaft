package com.example.greenleaf.greenleaf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Floor {
    @Id
    private String id;
    private String buildingId;
    private String name;
    private Integer floorNumber;
    private Integer rooms;
    private Double energyComsuptionn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(Integer floorNumber) {
        this.floorNumber = floorNumber;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public Double getEnergyComsuptionn() {
        return energyComsuptionn;
    }

    public void setEnergyComsuptionn(Double energyComsuptionn) {
        this.energyComsuptionn = energyComsuptionn;
    }
}
