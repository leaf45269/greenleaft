package com.example.greenleaf.greenleaf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Room {
    @Id
    private String id;
    private String floorId;
    private String name;
    private String category;
    private Integer roomNumber;
    private Double lightCapacity;
    private Double energyComsuption;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Double getLightCapacity() {
        return lightCapacity;
    }

    public void setLightCapacity(Double lightCapacity) {
        this.lightCapacity = lightCapacity;
    }

    public Double getEnergyComsuption() {
        return energyComsuption;
    }

    public void setEnergyComsuption(Double energyComsuption) {
        this.energyComsuption = energyComsuption;
    }
}
