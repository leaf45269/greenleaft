package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Building;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BuildingRepository extends MongoRepository<Building, String> {
    Optional<Building> findByName(String name);
}
