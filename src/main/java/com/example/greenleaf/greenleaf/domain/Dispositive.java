package com.example.greenleaf.greenleaf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document
public class Dispositive {
    @Id
    private String id;
    private String Category;
    private String dispositive;
    private String watts;
    private String kWatts;
    private ArrayList<String> Images;
    private String etiquetaId;
    private Boolean update;
    private Double priceDispositive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDispositive() {
        return dispositive;
    }

    public void setDispositive(String dispositive) {
        this.dispositive = dispositive;
    }

    public String getWatts() {
        return watts;
    }

    public void setWatts(String watts) {
        this.watts = watts;
    }

    public String getkWatts() {
        return kWatts;
    }

    public void setkWatts(String kWatts) {
        this.kWatts = kWatts;
    }

    public ArrayList<String> getImages() {
        return Images;
    }

    public void setImages(ArrayList<String> images) {
        Images = images;
    }

    public String getEtiquetaId() {
        return etiquetaId;
    }

    public void setEtiquetaId(String etiquetaId) {
        this.etiquetaId = etiquetaId;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Double getPriceDispositive() {
        return priceDispositive;
    }

    public void setPriceDispositive(Double priceDispositive) {
        this.priceDispositive = priceDispositive;
    }
}
