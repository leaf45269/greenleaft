package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Room;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoomRepository extends MongoRepository<Room, String> {
    Optional<Room> findByName(String name);
}
