package com.example.greenleaf.greenleaf.domain.repository;

import com.example.greenleaf.greenleaf.domain.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProjectRespository extends MongoRepository<Project, String> {
    Optional<Project> findByName(String name);
}