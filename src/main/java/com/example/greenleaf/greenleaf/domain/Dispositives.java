package com.example.greenleaf.greenleaf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Dispositives {
    @Id
    private String id;
    private String roomId;
    private String electrodomesticoId;
    private String hoursPerDay;
    private String hourPerMonth;
    private String dayComsuption;
    private String monthComsuption;
    private String dayPrice;
    private String monthPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getElectrodomesticoId() {
        return electrodomesticoId;
    }

    public void setElectrodomesticoId(String electrodomesticoId) {
        this.electrodomesticoId = electrodomesticoId;
    }

    public String getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(String hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }

    public String getHourPerMonth() {
        return hourPerMonth;
    }

    public void setHourPerMonth(String hourPerMonth) {
        this.hourPerMonth = hourPerMonth;
    }

    public String getDayComsuption() {
        return dayComsuption;
    }

    public void setDayComsuption(String dayComsuption) {
        this.dayComsuption = dayComsuption;
    }

    public String getMonthComsuption() {
        return monthComsuption;
    }

    public void setMonthComsuption(String monthComsuption) {
        this.monthComsuption = monthComsuption;
    }

    public String getDayPrice() {
        return dayPrice;
    }

    public void setDayPrice(String dayPrice) {
        this.dayPrice = dayPrice;
    }

    public String getMonthPrice() {
        return monthPrice;
    }

    public void setMonthPrice(String monthPrice) {
        this.monthPrice = monthPrice;
    }
}
