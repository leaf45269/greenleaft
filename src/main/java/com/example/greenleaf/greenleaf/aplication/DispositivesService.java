package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.DispositivosDTO;
import com.example.greenleaf.greenleaf.domain.Dispositives;
import com.example.greenleaf.greenleaf.domain.repository.DispositivesRepository;
import com.example.greenleaf.greenleaf.mappers.DispositivosMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DispositivesService {
    @Autowired
    DispositivesRepository repository;

    DispositivosMapper mapper = new DispositivosMapper();

    public Object crear(DispositivosDTO dto) {

        Dispositives dispositives = mapper.fromDispositivosDTOToDispositives(dto);
        return mapper.fromDispositivesToDispositivosDTO(repository.save(dispositives));
    }

    public List<DispositivosDTO> todos(){
        List<Dispositives> dispositives = repository.findAll();
        return mapper.fromCollectionList(dispositives);
    }

    public DispositivosDTO byId(String id){
        Dispositives dispositives = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromDispositivesToDispositivosDTO(dispositives);
    }
/*
    public DispositivosDTO byNombre(String name){
        Dispositives dispositives = repository.findByElectrod(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromDispositivesToDispositivosDTO(dispositives);
    }*/

    public DispositivosDTO actualizar(String id, DispositivosDTO dispositivosDTO){

        Dispositives dispositivesExist = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho objeto."));
        Dispositives dispositives = mapper.fromDispositivosDTOToDispositives(dispositivosDTO);
        dispositives.setId(dispositivesExist.getId());

        return mapper.fromDispositivesToDispositivosDTO(repository.save(dispositives));
    }

    public String borrar(String id){
        repository.deleteById(id);
        return id + " Delete Ok";
    }
}
