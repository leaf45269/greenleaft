package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.HabitacionDTO;
import com.example.greenleaf.greenleaf.domain.Room;
import com.example.greenleaf.greenleaf.domain.repository.RoomRepository;
import com.example.greenleaf.greenleaf.mappers.HabitacionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {
    @Autowired
    RoomRepository repository;

    HabitacionMapper mapper = new HabitacionMapper();

    public Object crear(HabitacionDTO dto) {
        Room room = mapper.fromHabitacionDTOToRoom(dto);
        return mapper.fromRoomToHabitacionDTO(repository.save(room));
    }

    public List<HabitacionDTO> todos(){
        List<Room> room = repository.findAll();
        return mapper.fromCollectionList(room);
    }

    public HabitacionDTO byId(String id){
        Room room = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromRoomToHabitacionDTO(room);
    }

    public HabitacionDTO byNombre(String name){
        Room room = repository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromRoomToHabitacionDTO(room);
    }

    public HabitacionDTO actualizar(String id, HabitacionDTO pisoDTO){

        Room roomExist = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho objeto."));
        Room room = mapper.fromHabitacionDTOToRoom(pisoDTO);
        room.setId(roomExist.getId());

        return mapper.fromRoomToHabitacionDTO(repository.save(room));
    }

    public String borrar(String id){
        repository.deleteById(id);
        return id + " Delete Ok";
    }
}
