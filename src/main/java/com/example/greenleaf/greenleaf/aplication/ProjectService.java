package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.ProyectoDTO;
import com.example.greenleaf.greenleaf.domain.Project;
import com.example.greenleaf.greenleaf.domain.repository.ProjectRespository;
import com.example.greenleaf.greenleaf.mappers.ProyectoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {
    @Autowired
    ProjectRespository projectRepository;

    ProyectoMapper mapper = new ProyectoMapper();

    public Object crear(ProyectoDTO proyectoDTO) {

        Project project = mapper.fromProyectoDTOToProject(proyectoDTO);
        return mapper.fromProjectToProyecto(projectRepository.save(project));
    }

    public List<ProyectoDTO> obtenerTodos(){
        List<Project> project = projectRepository.findAll();
        return mapper.fromCollectionList(project);
    }

    public ProyectoDTO proyectoById(String id){
        Project project = projectRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este proyecto."));
        return mapper.fromProjectToProyecto(project);
    }

    public ProyectoDTO proyectoByName(String name){
        Project project = projectRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este proyecto."));
        return mapper.fromProjectToProyecto(project);
    }

    public ProyectoDTO actualizar(String id, ProyectoDTO proyecto){

        Project projectExist = projectRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho proyecto."));
        Project project = mapper.fromProyectoDTOToProject(proyecto);
        project.setId(projectExist.getId());

        return mapper.fromProjectToProyecto(projectRepository.save(project));
    }

    public String borrar(String id){
        projectRepository.deleteById(id);
        return id + " Delete Ok";
    }

}
