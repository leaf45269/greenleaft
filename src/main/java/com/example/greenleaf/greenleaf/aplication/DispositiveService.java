package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.DispositivoDTO;
import com.example.greenleaf.greenleaf.domain.Dispositive;
import com.example.greenleaf.greenleaf.domain.repository.DispositiveRepository;
import com.example.greenleaf.greenleaf.mappers.DispositivoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DispositiveService {
    @Autowired
    DispositiveRepository repository;

    DispositivoMapper mapper = new DispositivoMapper();

    public Object crear(DispositivoDTO dto) {

        Dispositive dispositive = mapper.fromDispositivoDTOToDispositive(dto);
        return mapper.fromDispositiveToDispositivoDTO(repository.save(dispositive));
    }

    public List<DispositivoDTO> todos(){
        List<Dispositive> dispositives = repository.findAll();
        return mapper.fromCollectionList(dispositives);
    }

    public DispositivoDTO byId(String id){
        Dispositive dispositive = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromDispositiveToDispositivoDTO(dispositive);
    }

    public DispositivoDTO byNombre(String name){
        Dispositive dispositive = repository.findByDispositive(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromDispositiveToDispositivoDTO(dispositive);
    }

    public DispositivoDTO actualizar(String id, DispositivoDTO dispositivoDTO){

        Dispositive dispositiveExist = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho objeto."));
        Dispositive dispositive = mapper.fromDispositivoDTOToDispositive(dispositivoDTO);
        dispositive.setId(dispositiveExist.getId());

        return mapper.fromDispositiveToDispositivoDTO(repository.save(dispositive));
    }

    public String borrar(String id){
        repository.deleteById(id);
        return id + " Delete Ok";
    }
}
