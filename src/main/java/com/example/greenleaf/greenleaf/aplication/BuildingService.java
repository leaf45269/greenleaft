package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.ConstruccionDTO;
import com.example.greenleaf.greenleaf.domain.Building;
import com.example.greenleaf.greenleaf.domain.repository.BuildingRepository;
import com.example.greenleaf.greenleaf.mappers.ConstruccionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingService {
    @Autowired
    BuildingRepository repository;

    ConstruccionMapper mapper = new ConstruccionMapper();

    public Object crear(ConstruccionDTO dto) {

        Building building = mapper.fromConstruccionDTOToBuilding(dto);
        return mapper.fromBuildingToConstruccionDTO(repository.save(building));
    }

    public List<ConstruccionDTO> todos(){
        List<Building> buildings = repository.findAll();
        return mapper.fromCollectionList(buildings);
    }

    public ConstruccionDTO byId(String id){
        Building building = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromBuildingToConstruccionDTO(building);
    }

    public ConstruccionDTO byNombre(String name){
        Building building = repository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromBuildingToConstruccionDTO(building);
    }

    public ConstruccionDTO actualizar(String id, ConstruccionDTO construccion){

        Building buildingExist = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho objeto."));
        Building building = mapper.fromConstruccionDTOToBuilding(construccion);
        building.setId(buildingExist.getId());

        return mapper.fromBuildingToConstruccionDTO(repository.save(building));
    }

    public String borrar(String id){
        repository.deleteById(id);
        return id + " Delete Ok";
    }
}
