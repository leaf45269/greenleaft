package com.example.greenleaf.greenleaf.aplication;

import com.example.greenleaf.greenleaf.DTO.PisoDTO;
import com.example.greenleaf.greenleaf.domain.Floor;
import com.example.greenleaf.greenleaf.domain.repository.FloorRepository;
import com.example.greenleaf.greenleaf.mappers.PisoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FloorService {
    @Autowired
    FloorRepository repository;

    PisoMapper mapper = new PisoMapper();

    public Object crear(PisoDTO dto) {
        Floor floor = mapper.fromPisoDTOToFloor(dto);
        return mapper.fromFloorToPisoDTO(repository.save(floor));
    }

    public List<PisoDTO> todos(){
        List<Floor> floor = repository.findAll();
        return mapper.fromCollectionList(floor);
    }

    public PisoDTO byId(String id){
        Floor floor = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromFloorToPisoDTO(floor);
    }

    public PisoDTO byNombre(String name){
        Floor floor = repository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este objeto."));
        return mapper.fromFloorToPisoDTO(floor);
    }

    public PisoDTO actualizar(String id, PisoDTO pisoDTO){

        Floor floorExist = repository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho objeto."));
        Floor floor = mapper.fromPisoDTOToFloor(pisoDTO);
        floor.setId(floorExist.getId());

        return mapper.fromFloorToPisoDTO(repository.save(floor));
    }

    public String borrar(String id){
        repository.deleteById(id);
        return id + " Delete Ok";
    }
}
