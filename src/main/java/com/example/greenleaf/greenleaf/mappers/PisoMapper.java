package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.PisoDTO;
import com.example.greenleaf.greenleaf.domain.Floor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class PisoMapper {

    public Floor fromPisoDTOToFloor(PisoDTO piso){
        Floor floor = new Floor();
        floor.setId(piso.getId());
        floor.setBuildingId(piso.getCosntruccionId());
        floor.setEnergyComsuptionn(piso.getConsumoEnergia());
        floor.setFloorNumber(piso.getPiso());
        floor.setName(piso.getNombre());
        floor.setRooms(piso.getHabitaciones());

        return floor;
    }

    public PisoDTO fromFloorToPisoDTO (Floor floor){
        PisoDTO piso = new PisoDTO();
        piso.setId(floor.getId());
        piso.setConsumoEnergia(floor.getEnergyComsuptionn());
        piso.setCosntruccionId(floor.getBuildingId());
        piso.setHabitaciones(floor.getRooms());
        piso.setNombre(floor.getName());
        piso.setPiso(floor.getFloorNumber());

        return piso;

    }

    public List<PisoDTO> fromCollectionList(List<Floor> collection){
        if(collection == null){
            return null;
        }

        List<PisoDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Floor floor = (Floor) listTracks.next();
            list.add(fromFloorToPisoDTO(floor));
        }

        return list;

    }
}
