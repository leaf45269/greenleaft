package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.DispositivoDTO;
import com.example.greenleaf.greenleaf.domain.Dispositive;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class DispositivoMapper {
    public Dispositive fromDispositivoDTOToDispositive(DispositivoDTO dispositivo){
        Dispositive dispositive = new Dispositive();
        dispositive.setCategory(dispositivo.getCategoria());
        dispositive.setDispositive(dispositivo.getDispositivo());
        dispositive.setEtiquetaId(dispositivo.getEtiquetaId());
        dispositive.setId(dispositivo.getId());
        dispositive.setImages(dispositivo.getImagenes());
        dispositive.setkWatts(dispositivo.getPotenciaKWatts());
        dispositive.setPriceDispositive(dispositivo.getPrecio());
        dispositive.setUpdate(dispositivo.getActualizar());
        dispositive.setWatts(dispositivo.getPotenciaWatts());

        return dispositive;
    }

    public DispositivoDTO fromDispositiveToDispositivoDTO (Dispositive dispositive){
        DispositivoDTO dispositivo = new DispositivoDTO();
        dispositivo.setId(dispositive.getId());
        dispositivo.setActualizar(dispositive.getUpdate());
        dispositivo.setCategoria(dispositive.getCategory());
        dispositivo.setDispositivo(dispositive.getDispositive());
        dispositivo.setEtiquetaId(dispositive.getEtiquetaId());
        dispositivo.setImagenes(dispositive.getImages());
        dispositivo.setPotenciaKWatts(dispositive.getkWatts());
        dispositivo.setPotenciaWatts(dispositive.getWatts());
        dispositivo.setPrecio(dispositive.getPriceDispositive());

        return dispositivo;

    }

    public List<DispositivoDTO> fromCollectionList(List<Dispositive> collection){
        if(collection == null){
            return null;
        }

        List<DispositivoDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Dispositive dispositive = (Dispositive) listTracks.next();
            list.add(fromDispositiveToDispositivoDTO(dispositive));
        }

        return list;

    }
}

