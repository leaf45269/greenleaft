package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.HabitacionDTO;
import com.example.greenleaf.greenleaf.domain.Room;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class HabitacionMapper {
    public Room fromHabitacionDTOToRoom(HabitacionDTO habitacion){
        Room room = new Room();
        room.setId(habitacion.getId());
        room.setCategory(habitacion.getCategoria());
        room.setEnergyComsuption(habitacion.getConsumoEnergia());
        room.setLightCapacity(habitacion.getCapacidadLuz());
        room.setName(habitacion.getNombre());
        room.setFloorId(habitacion.getPisoId());
        room.setRoomNumber(habitacion.getHabitacion());

        return room;
    }

    public HabitacionDTO fromRoomToHabitacionDTO (Room room){
        HabitacionDTO habitacion = new HabitacionDTO();
        habitacion.setId(room.getId());
        habitacion.setCapacidadLuz(room.getLightCapacity());
        habitacion.setCategoria(room.getCategory());
        habitacion.setConsumoEnergia(room.getEnergyComsuption());
        habitacion.setNombre(room.getName());
        habitacion.setPisoId(room.getFloorId());
        habitacion.setHabitacion(room.getRoomNumber());

        return habitacion;

    }

    public List<HabitacionDTO> fromCollectionList(List<Room> collection){
        if(collection == null){
            return null;
        }

        List<HabitacionDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Room room = (Room) listTracks.next();
            list.add(fromRoomToHabitacionDTO(room));
        }

        return list;

    }
}
