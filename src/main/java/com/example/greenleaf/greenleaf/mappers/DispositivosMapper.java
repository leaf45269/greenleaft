package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.DispositivosDTO;
import com.example.greenleaf.greenleaf.domain.Dispositives;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class DispositivosMapper {
    public Dispositives fromDispositivosDTOToDispositives(DispositivosDTO dispositivos){
        Dispositives dispositives = new Dispositives();
        dispositives.setId(dispositivos.getId());
        dispositives.setDayComsuption(dispositivos.getConsumoDia());
        dispositives.setDayPrice(dispositivos.getPrecioDia());
        dispositives.setElectrodomesticoId(dispositivos.getElectrodomesticoId());
        dispositives.setHourPerMonth(dispositivos.getHorasUsoMes());
        dispositives.setHoursPerDay(dispositivos.getHorasUsoDia());
        dispositives.setMonthComsuption(dispositivos.getConsumoMes());
        dispositives.setMonthPrice(dispositivos.getPrecioMes());
        dispositives.setRoomId(dispositivos.getHabitacionId());

        return dispositives;
    }

    public DispositivosDTO fromDispositivesToDispositivosDTO (Dispositives dispositives){
        DispositivosDTO dispositivos = new DispositivosDTO();
        dispositivos.setId(dispositives.getId());
        dispositivos.setConsumoDia(dispositives.getDayComsuption());
        dispositivos.setConsumoMes(dispositives.getMonthComsuption());
        dispositivos.setElectrodomesticoId(dispositives.getElectrodomesticoId());
        dispositivos.setHabitacionId(dispositives.getRoomId());
        dispositivos.setHorasUsoDia(dispositives.getHoursPerDay());
        dispositivos.setHorasUsoMes(dispositives.getHourPerMonth());
        dispositivos.setPrecioDia(dispositives.getDayPrice());
        dispositivos.setPrecioMes(dispositives.getMonthPrice());

        return dispositivos;

    }

    public List<DispositivosDTO> fromCollectionList(List<Dispositives> collection){
        if(collection == null){
            return null;
        }

        List<DispositivosDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Dispositives dispositives = (Dispositives) listTracks.next();
            list.add(fromDispositivesToDispositivosDTO(dispositives));
        }

        return list;

    }
}
