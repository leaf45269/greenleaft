package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.ConstruccionDTO;
import com.example.greenleaf.greenleaf.domain.Building;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ConstruccionMapper {
    public Building fromConstruccionDTOToBuilding(ConstruccionDTO construccion){
        Building building = new Building();
        building.setId(construccion.getId());
        building.setAddres(construccion.getDireccion());
        building.setDate(construccion.getFecha());
        building.setElectricityBillId(construccion.getFacturaLuz());
        building.setFloors(construccion.getPisos());
        building.setName(construccion.getNombre());
        building.setRooms(construccion.getHabitaciones());
        building.setState(construccion.getEstado());
        building.setWaterBillId(construccion.getFacturaAgua());
        building.setGasBillId(construccion.getFacturaGas());

        return building;
    }

    public ConstruccionDTO fromBuildingToConstruccionDTO (Building building){
        ConstruccionDTO construccion = new ConstruccionDTO();
        construccion.setId(building.getId());
        construccion.setDireccion(building.getAddres());
        construccion.setEstado(building.getState());
        construccion.setFacturaAgua(building.getWaterBillId());
        construccion.setFacturaGas(building.getGasBillId());
        construccion.setFacturaLuz(building.getElectricityBillId());
        construccion.setFecha(building.getDate());
        construccion.setHabitaciones(building.getRooms());
        construccion.setPisos(building.getFloors());
        construccion.setNombre(building.getName());

        return construccion;

    }

    public List<ConstruccionDTO> fromCollectionList(List<Building> collection){
        if(collection == null){
            return null;
        }

        List<ConstruccionDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Building building = (Building) listTracks.next();
            list.add(fromBuildingToConstruccionDTO(building));
        }
        
        return list;

    }
}
