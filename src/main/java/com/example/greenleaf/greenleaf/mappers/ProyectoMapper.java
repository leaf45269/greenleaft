package com.example.greenleaf.greenleaf.mappers;

import com.example.greenleaf.greenleaf.DTO.ProyectoDTO;
import com.example.greenleaf.greenleaf.domain.Project;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ProyectoMapper {
    public Project fromProyectoDTOToProject(ProyectoDTO proyecto){
        Project project = new Project();
        project.setId(proyecto.getId());
        project.setName(proyecto.getNombre());
        project.setDescription(proyecto.getDescripcion());
        project.setLocation(proyecto.getLocalizacion());
        project.setSector(proyecto.getSector());

        return project;
    }

    public ProyectoDTO fromProjectToProyecto (Project project){
        ProyectoDTO proyecto = new ProyectoDTO();
        proyecto.setId(project.getId());
        proyecto.setDescripcion(project.getDescription());
        proyecto.setLocalizacion(project.getLocation());
        proyecto.setNombre(project.getName());
        proyecto.setSector(project.getSector());

        return proyecto;

    }

    public List<ProyectoDTO> fromCollectionList(List<Project> collection){
        if(collection == null){
            return null;
        }

        List<ProyectoDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Project project = (Project) listTracks.next();
            list.add(fromProjectToProyecto(project));
        }

        return list;

    }

}
