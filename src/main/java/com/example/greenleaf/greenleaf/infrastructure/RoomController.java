package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.HabitacionDTO;
import com.example.greenleaf.greenleaf.aplication.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/habitacion")
public class RoomController {
    @Autowired
    RoomService service;

    @GetMapping()
    public ResponseEntity<List<HabitacionDTO>> findAll() {
        return new ResponseEntity(service.todos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<HabitacionDTO> create(@RequestBody HabitacionDTO habitacionDTO) {
        return new ResponseEntity(service.crear(habitacionDTO), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<HabitacionDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.byId(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<HabitacionDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.byNombre(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<HabitacionDTO> update(@RequestBody HabitacionDTO habitacion,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, habitacion), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
