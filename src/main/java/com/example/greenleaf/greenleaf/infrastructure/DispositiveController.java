package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.DispositivoDTO;
import com.example.greenleaf.greenleaf.aplication.DispositiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dispositivo")
public class DispositiveController {
    @Autowired
    DispositiveService service;

    @GetMapping()
    public ResponseEntity<List<DispositivoDTO>> findAll() {
        return new ResponseEntity(service.todos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<DispositivoDTO> create(@RequestBody DispositivoDTO dispositivoDTO) {
        return new ResponseEntity(service.crear(dispositivoDTO), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<DispositivoDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.byId(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<DispositivoDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.byNombre(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<DispositivoDTO> update(@RequestBody DispositivoDTO dispositivoDTO,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, dispositivoDTO), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
