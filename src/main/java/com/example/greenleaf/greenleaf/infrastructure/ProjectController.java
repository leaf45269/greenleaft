package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.ProyectoDTO;
import com.example.greenleaf.greenleaf.aplication.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/proyectos")
public class ProjectController {
    @Autowired
    ProjectService service;

    @GetMapping()
    public ResponseEntity<List<ProyectoDTO>> findAll() {
        return new ResponseEntity(service.obtenerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<ProyectoDTO> create(@RequestBody ProyectoDTO proyecto) {
        return new ResponseEntity(service.crear(proyecto), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ProyectoDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.proyectoById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<ProyectoDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.proyectoByName(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<ProyectoDTO> update(@RequestBody ProyectoDTO proyecto,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, proyecto), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
