package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.DispositivosDTO;
import com.example.greenleaf.greenleaf.aplication.DispositivesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dispositivos")
public class DispositivesController {
    @Autowired
    DispositivesService service;

    @GetMapping()
    public ResponseEntity<List<DispositivosDTO>> findAll() {
        return new ResponseEntity(service.todos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<DispositivosDTO> create(@RequestBody DispositivosDTO dispositivosDTO) {
        return new ResponseEntity(service.crear(dispositivosDTO), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<DispositivosDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.byId(id), HttpStatus.OK);
    }
/*
    @GetMapping("/nombre/{name}")
    public ResponseEntity<DispositivosDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.byNombre(name), HttpStatus.OK);
    }*/

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<DispositivosDTO> update(@RequestBody DispositivosDTO dispositivosDTO,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, dispositivosDTO), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
