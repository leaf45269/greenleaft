package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.ConstruccionDTO;
import com.example.greenleaf.greenleaf.aplication.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/construccion")
public class BuildingController {
    @Autowired
    BuildingService service;

    @GetMapping()
    public ResponseEntity<List<ConstruccionDTO>> findAll() {
        return new ResponseEntity(service.todos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<ConstruccionDTO> create(@RequestBody ConstruccionDTO construccion) {
        return new ResponseEntity(service.crear(construccion), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ConstruccionDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.byId(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<ConstruccionDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.byNombre(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<ConstruccionDTO> update(@RequestBody ConstruccionDTO construccion,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, construccion), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
