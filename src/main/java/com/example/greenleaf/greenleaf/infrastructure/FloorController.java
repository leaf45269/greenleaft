package com.example.greenleaf.greenleaf.infrastructure;

import com.example.greenleaf.greenleaf.DTO.PisoDTO;
import com.example.greenleaf.greenleaf.aplication.FloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/piso")
public class FloorController {
    @Autowired
    FloorService service;

    @GetMapping()
    public ResponseEntity<List<PisoDTO>> findAll() {
        return new ResponseEntity(service.todos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<PisoDTO> create(@RequestBody PisoDTO piso) {
        return new ResponseEntity(service.crear(piso), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<PisoDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.byId(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<PisoDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.byNombre(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<PisoDTO> update(@RequestBody PisoDTO piso,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, piso), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
