package com.example.greenleaf.greenleaf.DTO;

public class PisoDTO {
    private String id;
    private String cosntruccionId;
    private String nombre;
    private Integer piso;                           //piso numero
    private Integer habitaciones;
    private Double consumoEnergia;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCosntruccionId() {
        return cosntruccionId;
    }

    public void setCosntruccionId(String cosntruccionId) {
        this.cosntruccionId = cosntruccionId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public Integer getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(Integer habitaciones) {
        this.habitaciones = habitaciones;
    }

    public Double getConsumoEnergia() {
        return consumoEnergia;
    }

    public void setConsumoEnergia(Double consumoEnergia) {
        this.consumoEnergia = consumoEnergia;
    }
}
