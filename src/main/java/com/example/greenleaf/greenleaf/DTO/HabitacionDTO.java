package com.example.greenleaf.greenleaf.DTO;

public class HabitacionDTO {
    private String id;
    private String pisoId;
    private String nombre;
    private String categoria;
    private Integer habitacion;                   //habitacion numero
    private Double capacidadLuz;
    private Double consumoEnergia;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPisoId() {
        return pisoId;
    }

    public void setPisoId(String pisoId) {
        this.pisoId = pisoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Integer habitacion) {
        this.habitacion = habitacion;
    }

    public Double getCapacidadLuz() {
        return capacidadLuz;
    }

    public void setCapacidadLuz(Double capacidadLuz) {
        this.capacidadLuz = capacidadLuz;
    }

    public Double getConsumoEnergia() {
        return consumoEnergia;
    }

    public void setConsumoEnergia(Double consumoEnergia) {
        this.consumoEnergia = consumoEnergia;
    }
}
