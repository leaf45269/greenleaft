package com.example.greenleaf.greenleaf.DTO;

public class DispositivosDTO {
    private String id;
    private String habitacionId;
    private String electrodomesticoId;
    private String horasUsoDia;
    private String horasUsoMes;
    private String consumoDia;
    private String consumoMes;
    private String precioDia;
    private String precioMes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHabitacionId() {
        return habitacionId;
    }

    public void setHabitacionId(String habitacionId) {
        this.habitacionId = habitacionId;
    }

    public String getElectrodomesticoId() {
        return electrodomesticoId;
    }

    public void setElectrodomesticoId(String electrodomesticoId) {
        this.electrodomesticoId = electrodomesticoId;
    }

    public String getHorasUsoDia() {
        return horasUsoDia;
    }

    public void setHorasUsoDia(String horasUsoDia) {
        this.horasUsoDia = horasUsoDia;
    }

    public String getHorasUsoMes() {
        return horasUsoMes;
    }

    public void setHorasUsoMes(String horasUsoMes) {
        this.horasUsoMes = horasUsoMes;
    }

    public String getConsumoDia() {
        return consumoDia;
    }

    public void setConsumoDia(String consumoDia) {
        this.consumoDia = consumoDia;
    }

    public String getConsumoMes() {
        return consumoMes;
    }

    public void setConsumoMes(String consumoMes) {
        this.consumoMes = consumoMes;
    }

    public String getPrecioDia() {
        return precioDia;
    }

    public void setPrecioDia(String precioDia) {
        this.precioDia = precioDia;
    }

    public String getPrecioMes() {
        return precioMes;
    }

    public void setPrecioMes(String precioMes) {
        this.precioMes = precioMes;
    }
}
