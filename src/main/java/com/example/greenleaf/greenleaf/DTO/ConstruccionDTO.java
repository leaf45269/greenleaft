package com.example.greenleaf.greenleaf.DTO;

public class ConstruccionDTO {
    private String id;
    private String nombre;
    private String direccion;
    private String fecha;
    private String facturaLuz;
    private String facturaAgua;
    private String facturaGas;
    private String estado;
    private Integer pisos;
    private Integer habitaciones;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFacturaLuz() {
        return facturaLuz;
    }

    public void setFacturaLuz(String facturaLuz) {
        this.facturaLuz = facturaLuz;
    }

    public String getFacturaAgua() {
        return facturaAgua;
    }

    public void setFacturaAgua(String facturaAgua) {
        this.facturaAgua = facturaAgua;
    }

    public String getFacturaGas() {
        return facturaGas;
    }

    public void setFacturaGas(String facturaGas) {
        this.facturaGas = facturaGas;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getPisos() {
        return pisos;
    }

    public void setPisos(Integer pisos) {
        this.pisos = pisos;
    }

    public Integer getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(Integer habitaciones) {
        this.habitaciones = habitaciones;
    }
}
