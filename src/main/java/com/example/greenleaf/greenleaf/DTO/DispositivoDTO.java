package com.example.greenleaf.greenleaf.DTO;

import java.util.ArrayList;

public class DispositivoDTO {
    private String id;
    private String categoria;
    private String dispositivo;
    private String potenciaWatts;
    private String potenciaKWatts;
    private ArrayList<String> imagenes;
    private String etiquetaId;
    private Boolean actualizar;
    private Double precio;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getPotenciaWatts() {
        return potenciaWatts;
    }

    public void setPotenciaWatts(String potenciaWatts) {
        this.potenciaWatts = potenciaWatts;
    }

    public String getPotenciaKWatts() {
        return potenciaKWatts;
    }

    public void setPotenciaKWatts(String potenciaKWatts) {
        this.potenciaKWatts = potenciaKWatts;
    }

    public ArrayList<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<String> imagenes) {
        this.imagenes = imagenes;
    }

    public String getEtiquetaId() {
        return etiquetaId;
    }

    public void setEtiquetaId(String etiquetaId) {
        this.etiquetaId = etiquetaId;
    }

    public Boolean getActualizar() {
        return actualizar;
    }

    public void setActualizar(Boolean actualizar) {
        this.actualizar = actualizar;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
